var canvas, ctx, img;
var frames=0, time=0, oTime;

function init () {
	console.log( "Init was called" );

	canvas = document.getElementById( "canvas" );
	// Get canvas 2D context
	ctx = canvas.getContext( '2d' );
	console.log( ctx );

	canvas.width = Math.max( 960, window.innerWidth );
	canvas.height = Math.max( 540, window.innerHeight );

	window.addEventListener( "resize", onResize, false );
	window.addEventListener( "click", clearCanvas, false );

	oTime = Date.now();
	time = Date.now();

	createImage();

	animate();
}

function createImage () {
	var r = 60;
	var s = 2*r;
	img = document.createElement( "canvas" );
	img.width = img.height = s;
	var ictx = img.getContext( "2d" );
	var grd = ictx.createRadialGradient( r, r, r/2, r, r, r );
	grd.addColorStop( 0, '#def95d' );
	grd.addColorStop( 0.5, 'rgba(222, 249, 188, 1.0)' );
	grd.addColorStop( 1, 'rgba(222, 249, 188, 0.0)' );

	ictx.rect( 0, 0, s, s );
	ictx.fillStyle = grd;
	ictx.fill();
}

function onResize ( event ) {
	canvas.width = Math.max( 960, window.innerWidth );
	canvas.height = Math.max( 540, window.innerHeight );
}

function clearCanvas ( event ) {
	ctx.clearRect( 0, 0, canvas.width, canvas.height );
}

function animate () {
	requestAnimationFrame( animate );
	
	draw();

	var currentTime = Date.now();
	var t = currentTime - oTime;
	var dt = currentTime - time;
	time = currentTime;

	frames++;

	document.getElementById( 'info' ).textContent = "Ellapsed: " + frames + "f / " + Math.round( t/1000 ) + "s | Frame duration: " + dt + "ms";
}

function draw () {
	var f = time * .001;
	var x = ( 1 + Math.sin( f ) )/2 * canvas.width;
	var y = ( 1 + Math.cos( f * .1 ) )/2 * canvas.height;

	//ctx.clearRect( 0, 0, canvas.width, canvas.height );
	ctx.drawImage( img, x, y );
}