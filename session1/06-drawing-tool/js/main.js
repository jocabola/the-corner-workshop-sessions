var canvas, ctx, img;
var frames=0, time=0, oTime;

var helpers, gfx;

var gui, optionsOpened = true;

var settings = {
	// define our app settings here
};

function init () {
	console.log( "Init was called" );

	canvas = document.getElementById( "canvas" );
	// Get canvas 2D context
	ctx = canvas.getContext( '2d' );
	console.log( ctx );

	helpers = {
		canvas: document.createElement( "canvas" ),
		ctx: null
	}

	gfx = {
		canvas: document.createElement( "canvas" ),
		ctx: null
	}

	helpers.ctx = helpers.canvas.getContext( '2d' );
	gfx.ctx = gfx.canvas.getContext( '2d' );

	window.addEventListener( "resize", onResize, false );
	onResize( null );

	window.addEventListener( "keydown", onKeyDown, false );

	oTime = Date.now();
	time = Date.now();

	initGUI();

	animate();
}

function initGUI () {
	gui = new dat.GUI( { autoPlace: false, width: 300 } );
	gui.domElement.removeChild( gui.__closeButton ); // REMOVE OPEN/CLOSE BUTTON
	document.getElementById( "controls-gui" ).appendChild( gui.domElement );
	// add our UI here
}

function onResize ( event ) {
	var w = Math.max( 960, window.innerWidth );
	var h = Math.max( 540, window.innerHeight );
	canvas.width = w;
	canvas.height = h;
	helpers.canvas.width = w;
	helpers.canvas.height = h;
	gfx.canvas.width = w;
	gfx.canvas.height = h;
}

function onKeyDown ( event ) {
	if ( event.keyCode == 32 ) {
		event.preventDefault();
		event.stopPropagation();
		optionsOpened = !optionsOpened;
		document.getElementById( "controls" ).style.right = optionsOpened ? "0px" : "-320px";
	}
}

function animate () {
	requestAnimationFrame( animate );
	
	draw();
}

function draw () {
	
}