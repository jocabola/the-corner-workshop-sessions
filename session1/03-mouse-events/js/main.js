var canvas, ctx, img, mouseover=false, canvasMouse;

var settings = {
	r: 80
}

function init () {
	console.log( "Init was called" );

	canvas = document.getElementById( "canvas" );
	// Get canvas 2D context
	ctx = canvas.getContext( '2d' );
	console.log( ctx );

	// load image
	img = new Image();
	img.src = 'img/computers.jpg';

	canvasMouse = {
		x: 0,
		y: 0
	}

	img.onload = function () {
		console.log( "image loaded" );
		draw();
		window.addEventListener( "mousemove", onMouseMove, false );
		/*canvas.addEventListener( "mouseover", onMouseOver, false );
		canvas.addEventListener( "mouseout", onMouseOut, false );*/
		initGUI();
	}
}

function initGUI () {
	var gui = new dat.GUI();
	gui.add( settings, "r", 40, 160 ).name( "Radius" );
}

function draw() {
	ctx.drawImage( img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height );

	if ( mouseover ) {
		// get scale factor
		var s = img.width / canvas.width;
		var r = settings.r;
		var r2 = 2*r;

		/// use save when using clip Path
		ctx.save();

		ctx.beginPath();
		ctx.arc( canvasMouse.x, canvasMouse.y, r, 0, 2*Math.PI );
		ctx.closePath();
		//ctx.fill();

		/// define this Path as clipping mask
		ctx.clip();

		var dx = canvasMouse.x-r;
		var dy = canvasMouse.y-r;
		var sx = canvasMouse.x*s;
		var sy = canvasMouse.y*s;

		if ( sx + r2 > img.width ) {
			sx = img.width - r2;
		}

		if ( sy + r2 > img.height ) {
			sy = img.height - r2;
		}

		/// draw the image
		//ctx.globalCompositeOperation = 'source-in';
		ctx.drawImage( img, sx, sy, img.width, img.height, dx, dy, img.width, img.height );
		//ctx.globalCompositeOperation = 'source-over';

		/// reset clip to default
		ctx.restore();
	}
}

function onMouseMove ( event ) {
	var x = event.clientX;
	var y = event.clientY;
	var info = document.getElementById( "info" );

	var cx = canvas.offsetLeft + canvas.clientLeft;
	var cy = canvas.offsetTop + canvas.clientTop;

	info.textContent = "Absolute Coordinates: [" + x + "," + y + "] / ";

	if ( ( x >= cx ) && ( x <= cx + canvas.width ) && ( y >= cy ) && ( y <= cy + canvas.height ) ) {
		// Mouse in
		info.textContent += "Canvas Coordinates: [" + (x-cx) + ", " + (y-cy) + "]";

		canvasMouse.x = x-cx;
		canvasMouse.y = y-cy;
		mouseover = true;
	}
	else {
		info.textContent += "Canvas Coordinates: Out of bounds";
		mouseover = false;
	}

	draw();
}

function onMouseOver ( event ) {
	mouseover = true;
}

function onMouseOut ( event ) {
	mouseover = false;
}