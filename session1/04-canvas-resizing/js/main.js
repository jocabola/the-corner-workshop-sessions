var canvas, ctx;

function init () {
	console.log( "Init was called" );

	canvas = document.getElementById( "canvas" );
	// Get canvas 2D context
	ctx = canvas.getContext( '2d' );
	console.log( ctx );

	canvas.width = Math.max( 960, window.innerWidth );
	canvas.height = Math.max( 540, window.innerHeight );

	drawCircle();

	window.addEventListener( "resize", onResize, false );
}

function drawCircle () {
	ctx.fillStyle = "#000";
	ctx.beginPath();
	ctx.arc( 100, 100, 80, 0, 2 * Math.PI );
	ctx.fill();
}

function onResize ( event ) {
	canvas.width = Math.max( 960, window.innerWidth );
	canvas.height = Math.max( 540, window.innerHeight );
	drawCircle();
}