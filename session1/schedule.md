# The Corner Workshops
### Session 01 - Schedule

1. Introduction + Sample applications: ~30 mins
2. Structure: ~20 mins
3. Introduction to Canvas Element: ~30 mins
4. Mouse Events: ~10 mins
5. Canvas resizing: ~10 mins
6. Animation: ~40 mins
7. BREAK (Food!!) ~30 mins
8. Let's create a drawing app with UI: till 10pm (~3hrs)
9. Optional beers at the pub if we feel like it :)