var canvas, ctx, img;

function init () {
	console.log( "Init was called" );

	canvas = document.getElementById( "canvas" );
	// Get canvas 2D context
	ctx = canvas.getContext( '2d' );
	console.log( ctx );

	// load image
	img = new Image();
	img.src = 'img/image.jpg';

	img.onload = function () {
		// Path Samples
		drawCircle();
		drawSquare();
		drawLine();

		// Bitmap Samples
		drawImageFullScaled();
		drawImageCropped();
	}
}

function drawCircle () {
	ctx.fillStyle = "#000";
	ctx.beginPath();
	ctx.arc( 100, 100, 40, 0, 2 * Math.PI );
	ctx.fill();
}

function drawSquare () {
	ctx.strokeStyle = "#f00";
	ctx.lineWidth = 5;
	ctx.beginPath();
	ctx.rect( 400, 80, 50, 50 );
	ctx.stroke();
}

function drawLine () {
	ctx.strokeStyle = "#f0f";
	ctx.lineWidth = 5;
	ctx.beginPath();
	ctx.moveTo( 450, 130 );
	ctx.lineTo( 380, 250 );
	ctx.lineTo( 800, 500 );
	ctx.stroke();

}

function drawImageFullScaled () {
	ctx.drawImage( img, 0, 0, img.width, img.height, 600, 100, img.width/2, img.height*0.5 );
}

function drawImageCropped () {
	ctx.drawImage( img, 100, 100, 200, 200, 100, 300, 200, 200 );
}